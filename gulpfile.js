const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const merge = require('merge-stream');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const zip = require('gulp-zip');

const rootDir = [
  'wp-content/themes/blank-theme/'
];
const assets = [
  rootDir + 'assets/',
];
const build = [
  rootDir + 'build/'
];
const cssSources = [
  assets + 'css/*.css'
];
const imgSources = [
  assets + 'img/*',
  !assets + 'img/*.svg'
];
const jsSources = [
  assets + 'js/*.js'
];
const jsVendorSources = [
  assets + 'js/vendor/*.js'
];
const phpBlockSources = [
  rootDir + 'blocks/*.php'
];
const phpFuncSources = [
  rootDir + 'php/*.php'
];
const rootSources = [
  rootDir + '*.php',
  rootDir + '*.css'
];
const sassSources = [
  assets + 'sass/*.sass'
];

function css() {
  var sassStream = gulp.src(sassSources)
    .pipe(sass())
    .pipe(concat('sass-files.sass'));

  // Concat and minify all .css files
  var cssStream = gulp.src(cssSources)
    .pipe(concat('css-files.css'));

  var mergedStream = merge(sassStream, cssStream)
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(concat('styles.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest(rootDir + 'css/'));
  return mergedStream;
}
exports.css = css;

function js() {
  return gulp.src(jsSources)
    .pipe(babel({presets: ['es2015']}))
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest(rootDir + 'js/'));
}
exports.js = js;

function jsVendor() {
  return gulp.src(jsVendorSources)
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest(rootDir + 'js/'));
}
exports.jsVendor = jsVendor;

function images() {
  return gulp.src(imgSources)
    .pipe(imagemin())
    .pipe(gulp.dest(rootDir + 'img/'));
}
exports.images = images;

function copyimagesSvg() {
  return gulp.src(assets + 'img/icons.svg')
    .pipe(gulp.dest(rootDir + 'img/'));
}
exports.copyimagesSvg = copyimagesSvg;

function root() {
  return gulp.src(rootSources)
    .pipe(gulp.dest(build + ''));
}
exports.root = root;

function phpBlock() {
  return gulp.src(phpBlockSources)
    .pipe(gulp.dest(build + 'blocks/'));
}
exports.phpBlock = phpBlock;

function phpFunc() {
  return gulp.src(phpFuncSources)
    .pipe(gulp.dest(build + 'php/'));
}
exports.phpFunc = phpFunc;

// Misc tasks to copy files to build dir
function copyCss() {
  return gulp.src(rootDir + 'css/*')
    .pipe(gulp.dest(build + 'css/'));
}
exports.copyCss = copyCss;

function copyImg() {
  return gulp.src(rootDir + 'img/*')
    .pipe(gulp.dest(build + 'img/'));
}
exports.copyImg = copyImg;

function copySvg() {
  return gulp.src(rootDir + 'img/*.svg')
    .pipe(gulp.dest(build + 'img/'));
}
exports.copySvg = copySvg;

function copyJs() {
  return gulp.src(rootDir + 'js/*')
    .pipe(gulp.dest(build + 'js/'));
}
exports.copyJs = copyJs;

// Zip build dir for production
function zipBuild() {
  return gulp.src([
    'wp-content/themes/blank-theme/build/*',
    'wp-content/themes/blank-theme/build/*/*'
  ])
    .pipe(zip('blank-theme.zip'))
    .pipe(gulp.dest('.'));
}
exports.zipBuild = zipBuild;

function watch(done) {
  gulp.watch(sassSources, css);
  gulp.watch(cssSources, css);
  gulp.watch(jsSources, js);
  gulp.watch(phpBlockSources, phpBlock);
  gulp.watch(phpFuncSources, phpFunc);
  done();
}

exports.default = gulp.series(
  exports.css,
  js,
  jsVendor,
  images,
  copyimagesSvg,
  phpBlock,
  phpFunc,
  root,
  copyCss,
  copyImg,
  copySvg,
  copyJs,
  zipBuild,
  watch
);
