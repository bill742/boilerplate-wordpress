<?php
/**
 * Header block
 *
 * Header block containing menu and social media links.
 *
 * @package    WordPress Boilerplate
 */

?>
<header>
  <div class="header-contact">
    <div class="container">
      <div class="contact">
        <a href="mailto:#" class="email">Email Me</a>
        <span class="phone">+00 (123) 456 78 90</span>
      </div>

      <ul class="header-social">
        <?php
          $social_options = array(
            'facebook'  => get_option( 'facebook' ),
            'twitter'   => get_option( 'twitter' ),
            'pinterest' => get_option( 'pinterest' ),
            'behance'   => get_option( 'behance' ),
            'dribble'   => get_option( 'dribble' ),
          );
        ?>

        <?php
          foreach ( $social_options as $key => $social_option ) {
            if ( $social_option !== '' ) {
        ?>
            <li class="social">
              <a href="<?php echo esc_html( $social_option ); ?>" target="_blank">
                <svg><use xlink:href="<?php echo esc_html( get_bloginfo( 'template_directory' ) ); ?>/img/icons.svg#<?php echo esc_html( $key ); ?>"></use>
                </svg>
              </a>
            </li>
        <?php
            }
          }
        ?>
      </ul>
    </div>
  </div>
  <div class="header-menu">
    <div class="container">
      <a href="<?php echo esc_html( get_bloginfo( 'wpurl' ) ); ?>" class="logo">
        <img src="<?php echo esc_html( get_bloginfo( 'template_directory' ) ); ?>/img/logo.png" alt="Logo">
      </a>
      <?php wp_nav_menu( array( 'theme_location' => 'header_menu' ) ); ?>
    </div>
  </div>
</header>
