<?php
/**
 * Footer block
 *
 * Footer block containing footer widgets and menu.
 *
 * @package    WordPress Boilerplate
 */

?>
<footer>
  <div class="footer-widgets">
    <div class="container">
      <?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
        <div id="footer-1" class="footer-widget footer-1" role="complementary">
          <?php dynamic_sidebar( 'footer-1' ); ?>
        </div>
      <?php endif; ?>
      <?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
        <div id="footer-2" class="footer-widget footer-2" role="complementary">
          <?php dynamic_sidebar( 'footer-2' ); ?>
        </div>
      <?php endif; ?>
      <?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
        <div id="footer-3" class="footer-widget footer-3" role="complementary">
          <?php dynamic_sidebar( 'footer-3' ); ?>
        </div>
      <?php endif; ?>
      <?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
        <div id="footer-4" class="footer-widget footer-4" role="complementary">
          <?php dynamic_sidebar( 'footer-4' ); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="footer-menu">
    <div class="container">
      <span class="footer-copy">© <span class="copyright"></span> acme co. All rights reserved.</span>
      <?php wp_nav_menu( array( 'theme_location' => 'footer_menu' ) ); ?>
    </div>
  </div>
</footer>
