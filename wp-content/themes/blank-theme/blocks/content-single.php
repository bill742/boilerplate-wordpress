<?php
/**
 * Content single block
 *
 * Content block for single pages.
 *
 * @package    WordPress Boilerplate
 */

?>

<div class="content container">
  <?php
  if ( has_post_thumbnail() ) {
    the_post_thumbnail();
  }
  ?>
  <h2 class="page-title"><?php the_title(); ?></h2>
  <p class="blog-post-meta"><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p>
  <? the_content(); ?>
</div>
