<?php
/**
 * Home content
 *
 * Layout for homepage
 *
 * @package    WordPress Boilerplate
 */

?>
<div class="container home-section section-1">
  <h2><?php the_field( 'section1-title' ); ?></h2>
  <p><?php the_field( 'section1-content' ); ?></p>

  <div class="sub-sections">
    <div class="sub-section first">
      <img src="<?php the_field( 'sub-section-1-image' ); ?>" alt="<?php the_field( 'sub-section-1-title' ); ?>">
      <h3><?php the_field( 'sub-section-1-title' ); ?></h3>
      <p><?php the_field( 'sub-section-1-copy' ); ?></p>
    </div>
    <div class="sub-section">
      <img src="<?php the_field( 'sub-section-2-image' ); ?>" alt="<?php the_field( 'sub-section-2-title' ); ?>">
      <h3><?php the_field( 'sub-section-2-title' ); ?></h3>
      <p><?php the_field( 'sub-section-2-copy' ); ?></p>
    </div>
    <div class="sub-section">
      <img src="<?php the_field( 'sub-section-3-image' ); ?>" alt="<?php the_field( 'sub-section-3-title' ); ?>">
      <h3><?php the_field( 'sub-section-3-title' ); ?></h3>
      <p><?php the_field( 'sub-section-3-copy' ); ?></p>
    </div>
  </div>
</div>

<div class="home-section section-2">
  <div class="container">
    <h2><?php the_field( 'section2-title' ); ?></h2>
    <p><?php the_field( 'section2-content' ); ?></p>

    <div class="sub-sections">
      <div class="sub-section first">
        <img src="<?php the_field( 'section-2-sub-section-1-image' ); ?>" alt="<?php the_field( 'works-featured-title-1' ); ?>">
        <div class="highlight-copy">
          <h3><?php the_field( 'section-2-sub-section-1-title' ); ?></h3>
          <p><?php the_field( 'works-featured-copy-1' ); ?></p>
        </div>
      </div>
      <div class="sub-section">
        <img src="<?php the_field( 'section-2-sub-section-2-image' ); ?>" alt="<?php the_field( 'works-featured-title-2' ); ?>">
        <div class="highlight-copy">
          <h3><?php the_field( 'section-2-sub-section-2-title' ); ?></h3>
          <p><?php the_field( 'works-featured-copy-2' ); ?></p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="home-section section-3">
  <div class="container">
    <h2><?php the_field( 'section3-title' ); ?></h2>
    <p><?php the_field( 'section3-content' ); ?></p>
    <button type="button" class="btn"><a href="<?php the_field( 'call-to-action-link' ); ?>"><?php the_field( 'call-to-action-text' ); ?></a></button>
  </div>
</div>
