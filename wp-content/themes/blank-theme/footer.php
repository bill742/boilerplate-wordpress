<?php
/**
 * Footer
 *
 * Common footer file for Blank Theme.
 *
 * @package WordPress Boilerplate
 */

?>

<?php require_once get_stylesheet_directory() . '/blocks/footer.php'; ?>

<?php wp_footer(); ?>
    <script>svg4everybody(); // run it now or whenever you are ready</script>
  </body>
</html>
