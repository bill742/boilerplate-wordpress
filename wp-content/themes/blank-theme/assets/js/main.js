// Copyright
const today = new Date();
const year = today.getFullYear();

const copyright = document.querySelector('.footer-copy .copyright');
copyright.innerHTML = year;

// Customize look of search box
// $('#searchsubmit').after(
//     '<button
//         type="submit",
//         id="#searchsubmit",
//         class="search-btn glyphicon glyphicon-search" ,aria-hidden="true"
//     >
//     </button>'
// ).hide();
