<?php
/**
 * Single page
 *
 * @package    WordPress Boilerplate
 */

?>

<?php get_header(); ?>

  <body class="single">

    <?php require_once get_stylesheet_directory() . '/blocks/header.php'; ?>

      <?php
        if ( have_posts() ) :
          while ( have_posts() ) :
            the_post();
            get_template_part( 'blocks/content-single', get_post_format() );
          endwhile;
        endif;
      ?>
      <a href="<?php get_permalink(); ?>">Read More</a>

<?php get_footer(); ?>
