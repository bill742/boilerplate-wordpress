<?php
/**
 * Index
 *
 * Theme index page.
 *
 * @package    WordPress Boilerplate
 */

?>
<?php get_header(); ?>

  <body>

    <?php require_once get_stylesheet_directory() . '/blocks/header.php'; ?>

    <?php
      if ( have_posts() ) {
        while ( have_posts() ) :
          the_post();
          if ( ! is_front_page() ) {
            get_template_part( 'blocks/content', get_post_format() );
          }
        endwhile;

        if ( is_front_page() ) {
          require_once get_stylesheet_directory() . '/blocks/home-content.php';
        }
      }

  get_footer();
