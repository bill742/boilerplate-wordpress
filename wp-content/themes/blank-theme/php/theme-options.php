<?php
/**
 * Theme options
 *
 * Theme options page in wp-admin.
 *
 * @package    WordPress Boilerplate
 */

?>

<?php
/**
 * Add menu item for custom settings page in wp-admin
 */
function custom_settings_add_menu() {
  add_menu_page( 'Custom Settings', 'Theme Options', 'manage_options', 'theme-options', 'custom_theme_settings_page', 'dashicons-admin-tools' );

  // Call register settings function.
  add_action( 'admin_init', 'register_theme_options_settings' );
}
add_action( 'admin_menu', 'custom_settings_add_menu' );

/**
 * Register theme setting fields.
 */
function register_theme_options_settings() {
  register_setting( 'custom-theme-settings-group', 'new_option_name' );
  register_setting( 'custom-theme-settings-group', 'some_other_option' );
  register_setting( 'custom-theme-settings-group', 'option_etc' );

  register_setting( 'custom-theme-settings-group', 'twitter' );
  register_setting( 'custom-theme-settings-group', 'facebook' );
  register_setting( 'custom-theme-settings-group', 'pinterest' );
  register_setting( 'custom-theme-settings-group', 'behance' );
  register_setting( 'custom-theme-settings-group', 'dribble' );
}

/**
 * Template for custom fields.
 */
function custom_theme_settings_page() {
?>
  <div class="wrap">
    <h1>Theme Options</h1>

    <form method="post" action="options.php">
      <?php
        settings_fields( 'custom-theme-settings-group' );
        do_settings_sections( 'custom-theme-settings-group' );
      ?>

      <h2>Social links</h2>
      <table class="form-table">
        <tr valign="top">
          <th scope="row">
            <label for="twitter">Twitter</label>
          </th>
          <td>
            <input type="text" name="twitter" id="twitter" value="<?php echo esc_html( get_option( 'twitter' ) ); ?>" size="30" />
          </td>
        </tr>
        <tr valign="top">
          <th scope="row">
            <label for="facebook">Facebook</label>
          </th>
          <td>
            <input type="text" name="facebook" id="facebook" value="<?php echo esc_html( get_option( 'facebook' ) ); ?>" size="30" />
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="pinterest">Pinterest</label>
          </th>
          <td>
            <input type="text" name="pinterest" id="pinterest" value="<?php echo esc_html( get_option( 'pinterest' ) ); ?>" size="30" />
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="behance">Behance</label>
          </th>
          <td>
            <input type="text" name="behance" id="behance" value="<?php echo esc_html( get_option( 'behance' ) ); ?>" size="30" />
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="dribble">Dribble</label>
          </th>
          <td>
            <input type="text" name="dribble" id="dribble" value="<?php echo esc_html( get_option( 'dribble' ) ); ?>" size="30" />
          </td>
        </tr>
      </table>

      <?php submit_button(); ?>

    </form>
  </div>
<?php
}
