<?php

?>

<div class="container test">
<?php
if ( get_option( 'section' ) ) {
  echo '<h1>test!!</h1>';

  // echo esc_html( get_option( 'section' ) );
} else {
  echo 'nope!';
}
?>

<?php if( have_rows('section', 'option') ): ?>

    <ul>

    <?php while( have_rows('section', 'option') ): the_row(); ?>

        <li><?php the_sub_field('title'); ?></li>

    <?php endwhile; ?>

    </ul>
<?php else:
  echo "no repeater!";

endif; ?>

</div>
