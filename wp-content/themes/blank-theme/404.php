<?php
/**
 * 404 page
 *
 * @package WordPress Boilerplate
 */

?>

<?php get_header(); ?>

  <body class="single">

    <?php require_once get_stylesheet_directory() . '/blocks/header.php'; ?>

    <div class="content container">

      <p>The page you were looking for could not be found.</p>

      <p><a href="/">Return to the homepage</a></p>

    </div>

<?php get_footer(); ?>
