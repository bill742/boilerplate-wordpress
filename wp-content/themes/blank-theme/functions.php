<?php
/**
 * Functions
 *
 * Functions file for Blank Theme
 *
 * @package    WordPress Boilerplate
 */

if ( ! function_exists( 'blanktheme_setup' ) ) :

  /**
   * Initial theme setup
   */
  function blanktheme_setup() {
    add_theme_support( 'automatic-feed-links' );

    load_theme_textdomain( 'blanktheme', get_template_directory() . '/languages' );

    add_theme_support( 'post-thumbnails' );
    add_theme_support(
      'post-formats',
      array(
        'aside',
        'gallery',
        'quote',
        'image',
        'video',
      )
    );
  }

endif;
add_action( 'after_setup_theme', 'blanktheme_setup' );

/**
 * Register menus
 */
function register_theme_menus() {
  register_nav_menus(
    array(
      'header_menu' => __( 'Header Menu' ),
      'footer_menu' => __( 'Footer Menu' ),
    )
  );
}
add_action( 'init', 'register_theme_menus' );

/**
 * Google Fonts
 */
function google_fonts() {
  $query_args = array(
    'family' => 'Source+Sans+Pro|Lato:400,900',
    'subset' => 'latin,latin-ext',
  );
  wp_register_style( 'google_fonts', add_query_arg( $query_args, '//fonts.googleapis.com/css' ), array(), 1.0, null );
}
add_action( 'wp_enqueue_scripts', 'google_fonts' );

/**
 * Enqueuing theme styles
 */
function blanktheme_enqueue_style() {
  wp_enqueue_style( 'main', esc_url( get_template_directory_uri() ) . '/css/styles.css', array(), 1.0, false );
}
add_action( 'wp_enqueue_scripts', 'blanktheme_enqueue_style' );

/**
 * Enqueuing theme scripts
 */
function blanktheme_enqueue_script() {
  wp_enqueue_script( 'main-js', '/wp-content/themes/blank-theme/js/scripts.js', '', 1.0, true );
  wp_enqueue_script( 'vendor', '/wp-content/themes/blank-theme/js/vendor.js', '', 1.0, true );

  wp_enqueue_script( 'html5shiv', 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', '', 3.7, true );
  wp_script_add_data( 'html5shiv', 'conditional', 'lte IE 9' );

  wp_enqueue_script( 'respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', '', 1.4, true );
  wp_script_add_data( 'respond', 'conditional', 'lte IE 9' );
}
add_action( 'wp_enqueue_scripts', 'blanktheme_enqueue_script' );

// WordPress Titles.
add_theme_support( 'title-tag' );

// Support Featured Images.
add_theme_support( 'post-thumbnails' );

/**
 * Register Widgets
 */
function blanktheme_widgets_init() {
  register_sidebar(
    array(
      'name'          => __( 'mega-menu1', 'blanktheme' ),
      'id'            => 'mega-menu-1',
      'description'   => __( 'Mega Menu block 1', 'blanktheme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    )
  );

  register_sidebar(
    array(
      'name'          => __( 'mega-menu2', 'blanktheme' ),
      'id'            => 'mega-menu-2',
      'description'   => __( 'Mega Menu block 2', 'blanktheme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    )
  );

  register_sidebar(
    array(
      'name'          => __( 'Footer 1', 'blanktheme' ),
      'id'            => 'footer-1',
      'description'   => __( 'Footer block 1', 'blanktheme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    )
  );

  register_sidebar(
    array(
      'name'          => __( 'Footer 2', 'blanktheme' ),
      'id'            => 'footer-2',
      'description'   => __( 'Footer block 2', 'blanktheme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    )
  );

  register_sidebar(
    array(
      'name'          => __( 'Footer 3', 'blanktheme' ),
      'id'            => 'footer-3',
      'description'   => __( 'Footer block 3', 'blanktheme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    )
  );

  register_sidebar(
    array(
      'name'          => __( 'Footer 4', 'blanktheme' ),
      'id'            => 'footer-4',
      'description'   => __( 'Footer block 4', 'blanktheme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    )
  );
}
add_action( 'widgets_init', 'blanktheme_widgets_init' );

// Theme options.
require 'php/theme-options.php';
