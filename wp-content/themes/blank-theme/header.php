<?php
/**
 * Header
 *
 * Common header file for Blank Theme including meta information.
 *
 * @package    WordPress Boilerplate
 */

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="<?php echo esc_html( get_bloginfo( 'description' ) ); ?>">
    <meta name="author" content="">
    <?php
      wp_head();
    ?>
    <?php require 'php/common.php'; ?>
  </head>
