# boilerplate-wordpress
A boilerplate for starting new WordPress projects. Comes with a bare-bones theme to get started as well as the following plugins:
* Advanced Custom Fields
* Ninja Forms
* Slide Anything

Includes gulp tasks designed to automatically:
* compile Sass files with auto-prefixer
* compile Javascript files
* minify all assets
* optimize images
* watch relevant directories for changes and auto reload

Update the variables in the gulp file to point to your preferred paths for your assets.
